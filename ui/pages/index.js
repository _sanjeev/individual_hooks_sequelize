import Heading from "../atoms/Heading";
import Paragraph from "../atoms/Paragraph";
import { HeaderCard, TopNavBar } from "molecules";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import ImageUploader from "atoms/ImageUploader";
import { useState } from "react";
import { addKid } from "services/kid.service";
import { uploadImageToS3Bucket, getSignedUrl } from "services/aws-uploader.service";

export default function Home() {
    const [imageUrl, setImageUrl] = useState();
    const [image, setImage] = useState();

    const {
        register,
        handleSubmit,
        formState: { errors, isValid },
    } = useForm({
        resolver: yupResolver(
            yup
                .object()
                .shape({
                    name: yup.string().required("Name is mandatory"),
                })
                .required()
        ),
    });

    const uploadImage = (event) => {
        if (event.target.files.length) {
            const imageUrl = URL.createObjectURL(event.target.files[0]);
            setImageUrl(imageUrl);
            setImage(event.target.files[0]);
        }
    };

    console.log("error", errors, isValid);

    const onSubmit = async (data) => {
        if (image?.name) {
            // const currentTime = new Date().getTime();
            // const currDateTime = new Date().toISOString() + currentTime;
            // const fileKey = `${currDateTime}.${image.name}`;
            // const uploadRes = await uploadImageToS3Bucket(fileKey, image);
            // console.log("🚀 ~ file: index.js:47 ~ onSubmit ~ uploadRes", uploadRes);

            //getSignedUrl
            const url = "https://kidimage.s3.ap-south-1.amazonaws.com/2023-02-13T07:36:38.662Z1676273798662.awsimg.jpg";
            const result = url.split("/");
            console.log("🚀 ~ file: index.js:50 ~ onSubmit ~ result", result[result.length - 1]);

            const fileKey = result[result.length - 1];

            const response = await getSignedUrl(fileKey);
            console.log("🚀 ~ file: index.js:55 ~ onSubmit ~ response", response);
        }
    };

    return (
        <div className="flex">
            <form onSubmit={handleSubmit(onSubmit)}>
                <ImageUploader uploadImage={uploadImage} uploadedImageUrl={imageUrl} />
                <input type="text" {...register("name")} placeholder="eeeeeeeeee" />
                <input type="submit" />
            </form>
        </div>
    );
}
