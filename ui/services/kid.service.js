import * as HttpService from "./http.service";
import * as URL from "./url.service";

export const addKid = (kidDetails) => {
    console.log("🚀 ~ file: kid.service.js:5 ~ addKid ~ kidDetails", kidDetails, URL.ADD_KID);
    return HttpService.postWithAuth(URL.ADD_KID, kidDetails);
};
