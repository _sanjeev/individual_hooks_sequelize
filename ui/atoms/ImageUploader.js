import React from "react";

function ImageUploader({ uploadImage, uploadedImageUrl }) {
    return (
        <div className=" flex justify-center items-center flex-col h-28 w-28 md:h-35.5 md:w-35.5 rounded-full cursor-pointer bg-gray-100 ">
            <label htmlFor="file">
                {uploadedImageUrl ? (
                    <div className="flex relative w-full h-full">
                        <img className="h-28 w-28 md:h-35.5 md:w-35.5 bg-cover rounded-full" src={uploadedImageUrl} alt="" />
                        {/* <img src="/images/Editblack.svg" className="absolute  right-0 bottom-6 h-5 w-5 rounded-full bg-secondary-600 flex justify-center items-center" /> */}
                    </div>
                ) : (
                    <div className="flex items-center justify-center flex-col ">
                        <img src="/images/Camera.png" />
                        <h1 className="text-black-normal font-normal text-sm mt-3">Upload photo</h1>
                    </div>
                )}

                <input id="file" className="hidden" type="file" placeholder="imageupload" onChange={(event) => uploadImage(event)} />
            </label>
        </div>
    );
}

export default ImageUploader;
