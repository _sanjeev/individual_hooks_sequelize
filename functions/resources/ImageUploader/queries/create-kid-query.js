const Models = require("models");

module.exports = class CreateKidQuery {
    constructor(details) {
        this.details = details;
    }

    get() {
        return Models.Kid.create(this.details);
    }
};
