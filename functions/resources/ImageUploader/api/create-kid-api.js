const Route = require("route");
const { logInfo } = require("lib/functional/logger");
const { respond, whenResult, composeResult, withArgs } = require("lib");
const Result = require("folktale/result");
const db = require("db/repository");
const uuid = require("uuid");
const CreateKidQuery = require("resources/ImageUploader/queries/create-kid-query");

async function post(req, res) {
    const { name, url } = req.body;
    logInfo("Request to create user", { name, url });
    const id = uuid.v4();
    const response = await db.execute(new CreateKidQuery({ id, name, url }));
    return respond(response, "Successfully create kid!", "Failed to create the kid!");
}

Route.withOutSecurity().noAuth().post("/kids", post).bind();
