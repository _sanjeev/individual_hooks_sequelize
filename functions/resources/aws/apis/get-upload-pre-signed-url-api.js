const Route = require("route");
const { logInfo } = require("lib/functional/logger");
const { respond } = require("lib");
const AWS = require("../s3-bucket");

const post = async (req) => {
    const { fileKey } = req.body;

    logInfo("Request to fetch presigned url", { fileKey });

    const response = await AWS.getUploadPreSignedUrl(fileKey);

    return respond(response, "Successfully fetched presigned url!", "Failed to fetch presigned url!");
};

Route.withOutSecurity().noAuth().post("/aws/get-upload-pre-signed-url", post).bind();

module.exports.post = post;
