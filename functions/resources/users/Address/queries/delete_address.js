const Models = require("models");

module.exports = class DeleteAddressQuery {
  constructor(userId, addressId) {
    this.details = {
      userId,
      addressId
    };
  }
  async get() {
      const user = await Models.User.findOne({
          where: {
              id: this.details.userId,
          }
      });
      //Getting a particular address using mixin.
    //   user.getAddresses({
    //       where : {
    //           id: this.details.addressId,
    //       }
    //   })

    const address = await Models.Address.findOne({
      where : {
        id : this.details.addressId,
      }
    })

    await user.removeAddress(address);
    return address.destroy();
  }
};
