const Models = require ('models');

module.exports = class UpdateAddressQuery {
    constructor (id, street, city, country, userId) {
        this.details = {
            id, 
            street,
            city,
            country,
            userId
        }
    }
    async get () {

        const address = await Models.Address.update ({
            street: this.details.street,
            city: this.details.city, 
            country: this.details.country
        }, {
            where: {
                id: this.details.id,
            }
        });
        
        return address;
    }
}