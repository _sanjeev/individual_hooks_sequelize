const Models = require("models");

module.exports = class CreateAddressQuery {
  constructor(id, street, city, country, userId) {
    this.details = {
      id,
      street,
      city,
      country,
      userId
    };
  }
  async get() {
    const user = await Models.User.findOne({
        where: {
            id: this.details.userId,
        }
    });
    return user.createAddress ({
        id:this.details.id,
        street: this.details.street,
        city: this.details.city,
        country: this.details.country,
    });
  }
};
