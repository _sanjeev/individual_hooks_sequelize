const Route = require("route");
const { logInfo } = require("lib/functional/logger");
const { respond, whenResult } = require("lib");
const Result = require("folktale/result");
const db = require("db/repository");
const uuid = require("uuid");
const CreateAddressQuery = require("resources/users/Address/queries/create_address.js");
const CreateAddressValidation = require("./../validation/create-address-validation");

async function post(req, res) {
  const userId = req.params.id;
  const { street, city, country } = req.body;
  logInfo("Request to create address", {
    street,
    city,
    country,
  });

  const id = uuid.v4();
  const validationResult = await CreateAddressValidation.validate({
    street,
    city,
    country,
  });

  const response = await whenResult(() => {
    return db.execute(
      new CreateAddressQuery(id, street, city, country, userId)
    );
  })(validationResult);
  return respond(
    response,
    "Successfully created addresses",
    "Failed to create addresses"
  );
}

Route.withOutSecurity().noAuth().post("/users/:id/addresses", post).bind();
// module.exports.post = post;
