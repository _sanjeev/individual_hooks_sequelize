const Route = require ('route');
const { logInfo } = require ('lib/functional/logger');
const { respond, composeResult } = require ('lib');
const Result = require ('folktale/result');
const Models = require("models");
const db = require ('db/repository');
const DeleteAddressQuery = require ('resources/users/Address/queries/delete_address.js');


const deleteUser = async(req, res) => {
    const userId = req.params.userId;
    const addressId = req.params.addressId;

    const response = await db.execute(new DeleteAddressQuery(userId, addressId));
    // const response = await composeResult(
    //     ()=>{
    //         return db.execute(new DeleteAddressQuery(userId, addressId));
    //     }
    // )();
    // await Models.Address.destroy({
    //     where: {
    //         id: addressId,
    //     }
    // {message: "Successfully delete the address"}
    // })
    return respond(response, 'Successfully delete address', 'Failed to delete address');
}

Route.withOutSecurity().noAuth().delete('/users/:userId/addresses/:addressId', deleteUser).bind();
module.exports.deleteUser = deleteUser;


