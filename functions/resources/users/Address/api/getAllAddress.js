const Route = require("route");
const { logInfo } = require("lib/functional/logger");
const { respond } = require("lib");
const Result = require("folktale/result");
const { Address, User } = require("models");
const db = require ('db/repository');
const GetAddressQuery = require ('resources/users/Address/queries/get_address.js');

async function get(req, res) {
  const id = req.params.id;
  logInfo("Request to get all address", {});
  const addresses = await db.execute(new GetAddressQuery(id));
  return respond(
    addresses,
    "Successfully get all address for a particular users",
    "Failed to fetched address for a particular user"
  );
}



Route.withOutSecurity().noAuth().get("/users/:id/addresses", get).bind();
