const {validate, notEmpty} = require ('validation');

const rule = {
    street: [[notEmpty, 'street should not be empty']],
    city: [[notEmpty, 'city shold not be empty']],
    country: [[notEmpty, 'country should not be empty']]
};

module.exports.validate = async data => validate(rule, data);