const { validate, notEmpty, hasLengthOf } = require("validation");
const findLength = require("../../../../lib/validations/findLength");


const rule = {
  aadharNumber: [
    [notEmpty, "aadharNumber should not be empty!"],
    [(value, obj) => {
        return hasLengthOf(12, value);
    }, 'aadharNumber Length must be of 12 digit'],
  ],
};

module.exports.validate = async (data) => validate(rule, data);
