const Route = require ('route');
const { logInfo } = require ('lib/functional/logger');
const { respond, composeResult } = require ('lib');
const Result = require ('folktale/result');
const db = require ('db/repository');
const uuid = require ('uuid');
const GetAadharQuery = require ('resources/users/Aadhar/queries/get_aadhar.js');

const getAadhar = (async(req, res) => {
    const id = req.params.id;
    logInfo ('Request to get aadhar for that user', {});
    // const userAadhar = await db.execute (new GetAadharQuery (id));
    const response = await composeResult (
        () => {
          return db.execute (new GetAadharQuery(id));
        }
      )();
    return respond(response, 'Successfully get the aadhar for a particular user', 'failed to fetch the aadhar of particular user');
})

Route.withOutSecurity().noAuth().get('/users/:id/aadhar', getAadhar).bind();
