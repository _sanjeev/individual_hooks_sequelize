const Models = require ('./../../../../models');

module.exports = class GetAadharQuery {
    constructor(userId) {
        this.details = {
            userId,
        }
    }
    async get() {
        const user = await Models.User.findOne ({
            where: {
                id: this.details.userId,
            }
        });
        return user.getAadhar();
    }
}