const Models = require("models");

module.exports = class CreateAadharQuery {
    constructor(details) {
        this.details = details;
    }
    async get() {
        // return Models.Aadhar.create({
        //   id: this.details.id,
        //   aadharNumber: this.details.aadharNumber,
        // });
        return await Models.Aadhar.create(this.details);
        // return user.createAadhar ({
        //     id: this.details.id,
        //     aadharNumber: this.details.aadharNumber
        // })
    }
};
