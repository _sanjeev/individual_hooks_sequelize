const { validate, notEmpty, shouldBeUuid, minValue, hasLengthOf, isEmail, isMobileNumber } = require("validation");

const findLength = require("./../../../../lib/validations/findLength");

const rule = {
    fullName: [
        [notEmpty, "Full Name should not be empty!"],
        [
            (value, obj) => {
                return minValue(2, findLength(value));
            },
            "fullName length should always greater than 2",
        ],
    ],
    countryCode: [
        [notEmpty, "countryCode Should not be empty!"],
        // [
        //   (value, obj) => {
        //     return hasLengthOf(2, JSON.stringify(value));
        //   },
        //   "countryCode Must be of length 2",
        // ],
    ],
};

module.exports.validate = async (data) => validate(rule, data);
