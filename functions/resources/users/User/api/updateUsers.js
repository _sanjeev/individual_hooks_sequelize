const Route = require("route");
const { logInfo } = require("lib/functional/logger");
const { respond, whenResult, composeResult } = require("lib");
const Result = require("folktale/result");
const Models = require("models");
const db = require("db/repository");
const UpdateUserQuery = require("resources/users/User/queries/update_user.js");
const UpdateUserValidation = require("./../validation/update-user-validation");

const updateValue = async (req, res) => {
    const id = req.params.id;

    // const response = await composeResult(() => {
    //     const user = db.execute(new UpdateUserQuery(id, fullName, countryCode));
    //     return user;
    // }, UpdateUserValidation.validate)({ fullName, countryCode });

    let obj = {
        fullName: "Hello Dude",
        countryCode: 65,
        Addresses: [
            {
                id: "044cb26b-26ea-4ce1-b489-e5a6fc51a89d",
                street: "street",
                city: "chapra, Saran Bihar",
                country: "India",
                userId: "ab4707cf-7446-4c0e-9f0d-01e3e951bca9",
            },
        ],
        aadharId: null,
    };

    const response = await db.execute(new UpdateUserQuery(obj, "ab4707cf-7446-4c0e-9f0d-01e3e951bca9"));

    return respond(response, "Successfully update the user", "Failed to update user");
};

Route.withOutSecurity().noAuth().put("/users/:id", updateValue).bind();
