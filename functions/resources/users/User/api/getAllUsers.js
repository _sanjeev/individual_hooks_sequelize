const Route = require ('route');
const { logInfo } = require ('lib/functional/logger');
const { respond, composeResult } = require ('lib');
const Result = require ('folktale/result');
const Models = require("models");
const db = require('./../../../../db/repository');
const GetAllUsersQuery = require('./../queries/get_all_user');

async function get() {
    logInfo('Request to create user', {});
    // const allUsers = await db.execute (new GetAllUsersQuery());
    const response = await composeResult (
        () => {
          return db.execute (new GetAllUsersQuery());
        },
      )();
    return respond(response, 'Successfully fetched all users', 'Failed to fetched user');
}


Route.withOutSecurity().noAuth().get('/users', get).bind();