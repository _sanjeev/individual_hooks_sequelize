const Models = require("models");

module.exports = class DeleteUserQuery {
  constructor(id) {
    this.details = {
      id,
    };
  }
  get() {
    return Models.User.destroy({
      where: {
        id: this.details.id,
      },
    });
  }
};
