const Models = require("models");

module.exports = class UpdateUserQuery {
    constructor(details, id) {
        this.details = details;
        this.id = id;
    }
    async get() {
        return Models.User.update(this.details, {
            where: {
                id: this.id,
            },
            include: [
                {
                    model: Models.Address,
                    individualHooks: true,
                },
            ],
        });
        // const user = await Models.User.update({
        //   fullName: this.details.fullName,
        //   countryCode: this.details.countryCode,
        // }, {
        //   where: {
        //     id: this.details.id
        //   }
        // });

        // const users = await Models.User.findOne({
        //   where : {
        //     id: this.details.id
        //   }
        // })

        // return users;
    }
};
