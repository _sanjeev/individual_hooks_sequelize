const Models = require("models");

module.exports = class CreateUserQuery {
    constructor(details) {
        this.details = details;
    }
    get() {
        return Models.User.create(this.details, {
            logging: console.log,
            include: [
                {
                    model: Models.Address,
                    individualHooks: true,
                },
            ],
        });
        return Models.User.destroy({
            where: {
                id: "ab4707cf-7446-4c0e-9f0d-01e3e951bca9",
            },
            logging: console.log,
            include: [
                {
                    model: Models.Address,
                    individualHooks: true,
                },
            ],
        });
        return Models.User.create(this.details, {
            logging: console.log,
            include: [
                {
                    model: Models.Address,
                    individualHooks: true,
                },
            ],
        });

        // return Models.User.findOrCreate({
        //     where: {
        //         id: "ab4707cf-7446-4c0e-9f0d-01e3e951bca9",
        //     },
        //     defaults: {
        //         id: "ab4707cf-7446-4c0e-9f0d-01e3e951bca9",
        //         fullName: "Name",
        //         countryCode: 91,
        //         Addresses: [
        //             {
        //                 id: "b12bb437-731f-41c8-9762-30b5b6fdc583",
        //                 street: "street",
        //                 city: "saran",
        //                 country: "india",
        //             },
        //         ],
        //         updatedAt: "2023-01-20T11:45:19.267Z",
        //         createdAt: "2023-01-20T11:45:19.267Z",
        //         aadharId: null,
        //     },
        //     include: [
        //         {
        //             model: Models.Address,
        //             individualHooks: true,
        //         },
        //     ],
        // });
    }
};
