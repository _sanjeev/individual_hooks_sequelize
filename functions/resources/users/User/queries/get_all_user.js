const Models = require("models");

module.exports = class GetAllUsersQuery {
  constructor() {
  }
  async get() {
    return await Models.User.findAll();
  }
};
