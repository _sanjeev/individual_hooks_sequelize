const Models = require("models");

module.exports = class MapUserRoleQuery {
  constructor(id) {
    this.details = {
      id,
    };
  }
  async get() {
    const roles = await Models.Role.findAll();
    const users = await Models.User.findAll();
    const userRoles = users.map (async(user) => {
        const data = roles.map((role) => {
            role.userId = user.id;
        })
        return data;
    })
    return userRoles;
  }
};
