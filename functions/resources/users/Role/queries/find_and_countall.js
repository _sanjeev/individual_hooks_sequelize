const Models = require("models");

module.exports = class Pagination {
  constructor(id) {
    this.details = {
      id,
    };
  }
  async get() {
    const {count, rows} = await Models.Role.findAndCountAll({
        where: {
            userId: this.details.id,
        }
    })
    console.log(count);
    // console.log(rows);
    return rows;
  }
};
