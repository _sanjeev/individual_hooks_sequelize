const Route = require ('route');
const { logInfo } = require ('lib/functional/logger');
const { respond } = require ('lib');
const Result = require ('folktale/result');
const db = require ('db/repository');
const uuid = require ('uuid');
const Models = require("models");
const Pagination = require ('resources/users/Role/queries/find_and_countall.js');

async function post (req, res) {
    const id = req.params.id;
    logInfo ('Request to find and countAll role', {})
    const response = await db.execute (new Pagination (id));
    return respond(response, 'Successfully find and count the Role', 'Failed to find and count the Role');
}

Route.withOutSecurity().noAuth().get('/users/:id/listroles', post).bind();
module.exports.post = post;