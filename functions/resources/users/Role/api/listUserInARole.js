const Route = require ('route');
const { logInfo } = require ('lib/functional/logger');
const { respond } = require ('lib');
const Result = require ('folktale/result');
const db = require ('db/repository');
const uuid = require ('uuid');
const Models = require("models");
const ListUserInARole = require ('resources/users/Role/queries/list_user_role.js');

async function post (req, res) {
    const id = req.params.id;

    logInfo ('Request to list role in a user', {})
    const response = await db.execute (new ListUserInARole (id));
    return respond(response, 'Successfully list the Role', 'Failed to list the Role');
}

Route.withOutSecurity().noAuth().get('/users/:id/roles', post).bind();
module.exports.post = post;