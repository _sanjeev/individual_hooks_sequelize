const Route = require("route");
const { logInfo } = require("lib/functional/logger");
const { respond, whenResult, composeResult } = require("lib");
const Result = require("folktale/result");
const db = require("db/repository");
const uuid = require("uuid");
const Models = require("models");
const CreateRoleQuery = require("resources/users/Role/queries/create_role.js");
const CreateRoleValidation = require("./../validation/create-role-validation");

async function post(req, res) {
    const userId = req.params.id;
    const { role } = req.body;
    logInfo("Request to create role", {});
    const id = uuid.v4();

    // const validationResult = await CreateRoleValidation.validate ({role});

    // const response = await whenResult (
    //     () => {
    //         return db.execute (new CreateRoleQuery (id, role, userId));
    //     }
    // )(validationResult);

    const response = await composeResult(() => {
        return db.execute(new CreateRoleQuery(id, role, userId));
    }, CreateRoleValidation.validate)({ role });
    console.log("🚀 ~ file: createRole.js:32 ~ post ~ response", response);

    return respond(response, "Successfully created Role", "Failed to create Role");
}

Route.withOutSecurity().noAuth().post("/users/:id/roles", post).bind();
module.exports.post = post;
