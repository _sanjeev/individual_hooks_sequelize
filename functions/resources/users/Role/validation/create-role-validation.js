const {validate, notEmpty} = require ('validation');

const rule = {
    role : [[notEmpty, "role shouldnot be empty"]],
};

module.exports.validate = async data => validate(rule, data);