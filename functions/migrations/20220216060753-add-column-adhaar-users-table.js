'use strict';
module.exports = {
    up: async (queryInterface, DataTypes) => {
        await queryInterface.addColumn('users', 'aadharId', {
            type: DataTypes.UUID,
            allowNull: true,
            // references: {model: 'aadhar_cards', key: 'id'}
        }, {})
    },
    down: async (queryInterface) => {
        await queryInterface.removeColumn('users', 'aadharId')
    }
};