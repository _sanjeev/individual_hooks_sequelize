module.exports = {
    async up(queryInterface, Sequelize) {
        await Promise.all([
            queryInterface.sequelize.query(
                `ALTER TABLE "addresses"
          DROP CONSTRAINT "addresses_userId_fkey",
          ADD CONSTRAINT "addresses_userId_fkey"
          FOREIGN KEY ("userId")
          REFERENCES "users"(id)
          ON DELETE CASCADE ON UPDATE CASCADE`
            ),
        ]);
    },

    async down(queryInterface, Sequelize) {
        /**
         * Add reverting commands here.
         *
         * Example:
         * await queryInterface.dropTable('users');
         */
    },
};
