"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
    class Kid extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    }
    Kid.init(
        {
            id: {
                allowNull: false,
                primaryKey: true,
                type: DataTypes.UUID,
            },
            name: DataTypes.STRING,
            url: DataTypes.STRING,
        },
        {
            sequelize,
            modelName: "Kid",
        }
    );
    return Kid;
};
